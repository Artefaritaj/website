+++
abstract = "A desired property of secure programs is control flow integrity (CFI): an attacker must not be able to alter how instructions are chained as specified in the program. Numerous techniques try to achieve this property with various trade-offs. But to achieve fine-grained CFI, one is required to extract a precise control flow graph (CFG), describing how instructions are chained together. Unfortunately it is not achievable in general. In this paper, we propose a way to overcome this impossibility result by restricting the instruction set architecture (ISA) semantics. We show that forbidding indirect jumps unlocks a precise CFG extraction for all acceptable programs. We discuss the implications and limitations of the new semantics and argue for the adoption of restricted ISAs for security-related computation."
authors = ["Alexandre Gonzalvez", "Ronan Lashermes"]
date = "2019-12-10"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *The 9th Software Security, Protection and Reverse Engineering Workshop*"
publication_short = "SSPREW2019"
selected = false
title = "A case against indirect jumps for secure programs"
url_code = ""
url_dataset = ""
url_pdf = "papers/SSPREW2019.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++ 
