+++
abstract = "In the realm of fault injection (FI), electromagnetic fault injection (EMFI) attacks have garnered significant attention, particularly for their effectiveness against embedded systems with minimal setup. These attacks exploit vulnerabilities with ease, underscoring the importance of comprehensively understanding EMFI. Recent studies have highlighted the impact of EMFI on phase-locked loops (PLLs), uncovering specific clock glitches that induce faults. However, these studies lack a detailed explanation of how these glitches translate into a specific fault model. Addressing this gap, our research investigates the physical fault model of synchronous clock glitches (SCGs), a clock glitch injection mechanism likely to arise from EMFI interactions within the clock network. Through an integrated approach combining experimental and simulation techniques, we critically analyze the adequacy of existing fault models, such as the Timing Fault Model and the Sampling Fault Model in explaining SCGs. Our findings reveal specific failure modes in D flip-flops (DFFs), contributing to a deeper understanding of EMFI effects and aiding in the development of more robust defensive strategies against such attacks."
authors = ["Amélie Marotta", "Ronan Lashermes", "Guillaume Bouffard", "Olivier Sentieys", "Rachid Dafali"]
date = "2024-04-08"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In the *International Workshop on Constructive Side-Channel Analysis and Secure Design*"
publication_short = "COSADE2024"
selected = false
title = "Characterizing and Modeling Synchronous Clock-Glitch Fault Injection"
url_code = ""
url_dataset = ""
url_pdf = "papers/COSADE2024.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++ 
