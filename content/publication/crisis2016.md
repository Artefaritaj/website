+++
abstract = "Elliptic Curve Cryptography (ECC) is becoming unavoidable, and should be used for public key protocols. It has gained increasing acceptance in practice due to the significantly smaller bit size of the operands compared to RSA for the same security level. Most protocols based on ECC imply the computation of a scalar multiplication. ECC can be performed in affine, projective, Jacobian or others models of coordinates. The arithmetic in a finite field constitutes the core of ECC Public Key Cryptography. This paper discusses an efficient hardware implementation of scalar multiplication in Jacobian coordinates by using the Coarsely Integrated Operand Scanning method (CIOS) of Montgomery Modular Multiplication (MMM) combined with an effective systolic architecture designed with a two-dimensional array of Processing Elements (PE). As far as we know this is the first implementation of such a design for large prime fields. The proposed architectures are designed for Field Programmable Gate Array (FPGA) platforms. The objective is to reduce the number of clock cycles of the modular multiplication, which implies a good performance for ECC. The presented implementation results focuses on various security levels useful for cryptography. This architecture have been designed in order to use the flexible DSP48 on Xilinx FPGAs. Our architecture for MMM is scalable and depends only on the number and size of words."
authors = ["Amine Mrabet", "Nadia El-Mrabet", "Ronan Lashermes", "Jean-Baptiste Rigaud", "Belgacem Bouallegue", "Sihem Mesnager", "Mohsen Machhout"]
date = "2016-09-05"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *the 11th International Conference on Risks and Security of Internet and Systems*"
publication_short = "CRiSIS2016"
selected = false
title = "High-performance Elliptic Curve Cryptography by Using the CIOS Method for Modular Multiplication"
url_code = ""
url_dataset = ""
url_pdf = "papers/CRiSIS2016.pdf"
url_project = ""
url_slides = "slides/CRiSIS2016.pdf"
url_video = ""
+++
