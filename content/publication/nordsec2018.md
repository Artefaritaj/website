+++
abstract = "Even if a software is proven sound and secure, an attacker can still insert vulnerabilities with fault attacks. In this paper, we propose HAPEI, an Instruction Set Randomization scheme to guarantee Program Execution Integrity even in the presence of hardware fault injection. In particular, we propose a new solution to the multi-predecessors problem. This scheme is then implemented as a hardened CHIP-8 virtual machine, able to ensure program execution integrity, to prove the viability and to explore the limits of HAPEI."
authors = ["Ronan Lashermes", "Hélène Le Bouder", "Gaël Thomas"]
date = "2018-11-28"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *The 23rd Nordic Conference on Secure IT Systems*"
publication_short = "NordSec2018"
selected = false
title = "Hardware-Assisted Program Execution Integrity: HAPEI"
url_code = ""
url_dataset = ""
url_pdf = "papers/NordSec2018.pdf"
url_project = ""
url_slides = "slides/NordSec18.pdf"
url_video = ""
+++
