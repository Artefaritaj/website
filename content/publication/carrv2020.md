+++
abstract = "The rising number of attacks targeting processors at micro-architecture level encourages more research on hardware level solutions. In this position paper, we specify a new RV32S “secure” instruction setarchitecture (ISA) derived from the RV32I RISC-V ISA. We propose modifications in the ISA to prevent timing side-channels, strengthen control flow integrity and ensure micro-architectural state isolation. The goal is to provide a new minimal hardware/software approach through which software attacks exploiting hardware vulnerabilities can be circumvented."
authors = ["Mathieu Escouteloup", "Jacques Fournier", "Jean-Louis Lanet", "Ronan Lashermes"]
date = "2020-05-29"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In the *Fourth Workshop on Computer Architecture Research with RISC-V*"
publication_short = "CARRV2020"
selected = false
title = "Recommendations for a radically secure ISA"
url_code = ""
url_dataset = ""
url_pdf = "papers/CARRV2020.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++ 
