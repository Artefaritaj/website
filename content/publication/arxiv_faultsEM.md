+++
abstract = "Electromagnetic fault injection (EMFI) is a well known technique used to disturb the behaviour of a chip for weakening its security. These attacks are mostly done on simple microcontrollers. On these targets, the fault effects are relatively simple and understood. Exploiting EMFI on modern system-on-chips (SoCs), the fast and complex chips ubiquitous today, requires to understand the impact of such faults. In this paper, we propose an experimental setup and a forensic process to create exploitable faults and assess their impact on the SoC micro-architecture. On our targeted SoC (a BCM2837), the observed behaviours are radically different to what were obtained with state-of-the-art fault injection attacks on microcontrollers. SoC subsystems (L1 caches, L2 cache, memory management unit (MMU)) can be individually targeted leading to new fault models. We also highlight the differences in the fault impact with and without an operating system (OS). This shows the importance of the software layers in the exploitation of a fault. With this work, we demonstrate that the complexity and the speed of SoCs do not protect them against hardware fault attacks. To conclude our work, we introduce countermeasures to protect the SoC caches and MMU against EMFI attacks based on the disclosed faults effects. "
authors = ["Thomas Trouchkine", "Sébanjila Kevin Bukasa", "Mathieu Escouteloup", "Ronan Lashermes", "Guillaume Bouffard"]
date = "2019-10-25"
image = ""
image_preview = ""
math = false
publication_types = ["0"]
publication = "In *arXiv*"
publication_short = "arXiv:1910.11566"
selected = false
title = "Electromagnetic fault injection against a System-on-Chip, toward new micro-architectural fault models"
url_code = ""
url_dataset = ""
url_pdf = "papers/FaultsSOC.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++
