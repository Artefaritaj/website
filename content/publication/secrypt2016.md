+++
abstract = "This paper presents the first side channel analysis from electromagnetic emissions on VERIFY PIN algorithms. To enter a PIN code, a user has a limited number of trials. Therefore the main difficulty of the attack is to succeed with very few traces. More precisely, this work implements a template attack and experimentally verifies its success rate. It becomes a new real threat, and it is feasible on a low cost and portable platform. Moreover, this paper shows that some protections for VERIFY PIN algorithms against fault attacks introduce new vulnerabilities with respect to side channel analysis."
authors = ["Hélène Le Bouder", "Thierno Barry", "Damien Couroussé", "Jean-Louis Lanet", "Ronan Lashermes"]
date = "2016-07-26"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *the 13th International Conference on Security and Cryptography*"
publication_short = "SECRYPT2016"
selected = false
title = "A template attack against VERIFY PIN algorithms"
url_code = ""
url_dataset = ""
url_pdf = "papers/SECRYPT2016.pdf"
url_project = ""
url_slides = "slides/SECRYPT2016.pdf"
url_video = ""
+++
