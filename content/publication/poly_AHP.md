+++
abstract = "Support du cours Advanced Hardware Protection donné au M2 Cyberschool à l'université de Rennes"
authors = ["Ronan Lashermes", "Hélène Le Bouder"]
date = "2025-01-07"
image = ""
image_preview = ""
math = false
publication_types = ["0"]
publication = "Cours AHP"
publication_short = "Cours AHP"
selected = true
title = "Sécurité matérielle : l'interface matériel/logiciel"
url_code = ""
url_dataset = ""
url_pdf = "others/poly_AHP.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++
