+++
abstract = "Numerous timing side-channels attacks have been proposed in the recent years, showing that all shared states inside the microarchitecture are potential threats. Previous works have dealt with this problem by considering those “shared states” separately and not by looking at the system as a whole. In this paper, instead of reconsidering the problematic shared resources one by one, we lay out generic guidelines to design complete cores immune to microarchitectural timing information leakage. Two implementations are described using the RISC-V ISA with a simple extension. The cores are evaluated with respect to performances, area and security, with a new open-source benchmark assessing timing leakages. We show that with this “generic” approach, designing secure cores even with complex features such as simultaneous multithreading is possible. We discuss about the trade-offs that need to be done in that respect regarding the microarchitecture design."
authors = ["Mathieu Escouteloup", "Ronan Lashermes", "Jacques Fournier", "Jean-Louis Lanet"]
date = "2021-11-11"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *the 20th Smartcard Research and Advanced Application Confirence (CARDIS), 2021*"
publication_short = "CARDIS2021"
selected = false
title = "Under the dome: preventing hardware timing information leakage"
url_code = ""
url_dataset = ""
url_pdf = "papers/CARDIS2021.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++
