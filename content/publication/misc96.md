+++
abstract = "Rendre vulnérable un code sûr, réaliser une porte dérobée indétectable, voici quelques possibilités des attaques par faute. Faites chauffer votre amplificateur RF, nous allons faire des étincelles ! Licence CC BY-NC-ND."
authors = ["Ronan Lashermes"]
date = "2018-03-01"
image = ""
image_preview = ""
math = false
publication_types = ["0"]
publication = "MISC n°96"
publication_short = "MISC96"
selected = false
title = "Attaques par fautes"
url_code = ""
url_dataset = ""
url_pdf = "papers/MISC96.pdf"
url_project = ""
url_slides = ""
url_video = ""


+++
