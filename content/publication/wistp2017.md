+++
abstract = "Side-channel attacks (SCA) exploit the reification of a computation through its physical dimensions (current consumption, EM emission, ... ). Focusing on Elecromagnetic analyses (EMA), such analyses have mostly been considered on low-end devices: smartcards and microcontrollers. In the wake of recent works, we propose to analyze the effects of a modern microarchitecture on the efficiency of EMA (here Correlation Power Analysis and template attacks). We show that despite the difficulty to synchronize the measurements, the speed of the targeted core and the activity of other cores on the same chip can still be accommodated. Finally, we confirm that enabling the secure mode of TrustZone (a hardware-assisted software countermeasure) has no effect whatsoever on the EMA efficiency. Therefore, critical applications in TrustZone are not more secure than in the normal world with respect to EMA, in accordance with the fact that it is not a countermeasure against physical attacks. For the best of our knowledge this is the first application of EMA against TrustZone."
authors = ["Sebanjila Kevin Bukasa", "Ronan Lashermes", "Hélène Le Bouder", "Jean-Louis Lanet", "Axel Legay"]
date = "2017-09-28"
image = ""
image_preview = ""
math = false
publication_types = ["1"]
publication = "In *the 11th International Conference on Information Security Theory and Practice*"
publication_short = "WISTP2017"
selected = false
title = "How TrustZone could be bypassed: Side-Channel Attacks on a modern System-on-Chip"
url_code = ""
url_dataset = ""
url_pdf = "papers/WISTP2017.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++
