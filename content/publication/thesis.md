+++
abstract = "Pairings are cryptographic algorithms allowing new protocols for public-key cryptography. After a decade of research which led to a dramatic improvement of the computation speed of pairings, we focused on the security of pairing implementations.For that purpose, we evaluated the resistance to fault attacks. We have sent electromagnetic pulses in the chip computing a pairing at a precise instant. It allowed us to recover the cryptographic secret which should be protected in the computation. Our study was both theoretical and practical; we did implement actual fault attacks. Finally, we proposed countermeasures in order to protect the algorithm in the future."
authors = ["Ronan Lashermes"]
date = "2014-09-29"
image = ""
image_preview = ""
math = false
publication_types = ["7"]
publication = "PhD Thesis"
publication_short = "PhD Thesis"
selected = true
title = "On the security of pairing implementations"
url_code = ""
url_dataset = ""
url_pdf = "papers/Thesis.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++
