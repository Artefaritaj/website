+++
abstract = "Entrer son code PIN pour utiliser sa carte bancaire ou déverrouiller son téléphone portable est devenu un geste quotidien. L’objet doit vérifier que le code proposé est correct. Comment implémenter cette vérification ? Cela semble être une simple comparaison de deux tableaux de données. Détrompez-vous ! Les attaques physiques vont nous mener la vie dure. Licence CC BY-NC-ND."
authors = ["Ronan Lashermes", "Hélène Le Bouder"]
date = "2018-05-01"
image = ""
image_preview = ""
math = false
publication_types = ["0"]
publication = "MISC"
publication_short = "MISC97"
selected = false
title = "Vérifier un code PIN"
url_code = ""
url_dataset = ""
url_pdf = "papers/MISC97.pdf"
url_project = ""
url_slides = ""
url_video = ""


+++
