+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Embedded systems",
    "Hardware/Software interface",
    "Hardware security",
    "Physical attacks",
    "Microarchitectural security"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in computer science"
  institution = "[CEA-Leti/-TECH](http://www.leti-cea.com/cea-tech/leti/english) and [UVSQ](http://welcome.uvsq.fr)"
  year = 2014

[[education.courses]]
  course = "Engineering School"
  institution = "[Grenoble-INP Phelma](http://phelma.grenoble-inp.fr/welcome/school-of-engineering-in-physics-electronics-and-materials-science-234605.kjsp)"
  year = 2011

# List your professional experiences

[[experience.descriptions]]
  role = "Research Engineer"
  institution = "[Inria](https://www.inria.fr/en/centre/rennes)"
  year = "2017-"

[[experience.descriptions]]
  role = "Post-doc"
  institution = "[Inria](https://www.inria.fr/en/centre/rennes)"
  year = "2016-2017"

[[experience.descriptions]]
  role = "Research Engineer"
  institution = "[Secure-IC](http://www.secure-ic.com)"
  year = "2014-2016"

[[experience.descriptions]]
  role = "PhD in computer science"
  institution = "[CEA-Leti/-TECH](http://www.leti-cea.com/cea-tech/leti/english) and [UVSQ](http://welcome.uvsq.fr)"
  year = 2014

+++

# Biography

Currently a research engineer at [Inria](https://www.inria.fr/en/centre/rennes), I am interested in the impact of abstraction barriers (software/mathematics, hardware/software) on the security of computing systems.

Physical attacks (side-channels and fault injection) are the most obvious manifestations of this impact.

In the LHS lab, our physical attack benches allow us to exhibit vulnerabilities that are not achievable with software only.

Microarchitectural security is my second research axis, where I try to conciliate better core performance and security.
