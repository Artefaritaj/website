+++
# Date this page was created.
date = "2017-05-09"

# Project title.
title = "Faustine"

# Project summary to display on homepage.
summary = "The data (code, docs, ...) for the INRIA Faustine platform for Electromagnetic fault injection"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = ""

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["physical attacks", "electromagnetic fault injection", "fi"]

# Optional external URL for project (replaces project detail page).
external_link = "https://gitlab.inria.fr/rlasherm/Faustine"

# Does the project detail page use math formatting?
math = false
+++
