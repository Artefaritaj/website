+++
# Date this page was created.
date = "2017-07-03"

# Project title.
title = "Sparkbench"

# Project summary to display on homepage.
summary = "Sparkbench is a tool to perform physical attacks. It will orchestrate the apparatus and data retrieval. With automatic error recovery."

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = ""

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["physical attacks", "electromagnetic fault injection", "fi"]

# Optional external URL for project (replaces project detail page).
external_link = "https://gitlab.com/Artefaritaj/Sparkbench"

# Does the project detail page use math formatting?
math = false
+++
