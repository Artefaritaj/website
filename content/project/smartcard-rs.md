+++
# Date this page was created.
date = "2017-01-18"

# Project title.
title = "smartcard-rs"

# Project summary to display on homepage.
summary = "A wrapper library to access the pcsc lib from the rust language"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = ""

# Optional image to display on project detail page (relative to `static/img/` folder).
image = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["rust", "smartcard"]

# Optional external URL for project (replaces project detail page).
external_link = "https://gitlab.com/Artefaritaj/smartcard-rs"

# Does the project detail page use math formatting?
math = false

+++
