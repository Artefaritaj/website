+++
date = "2019-05-10"
math = false
title = "Do Not Trust Modern System-on-Chips"
abstract = ""
abstract_short = ""
event = "Séminaire Sécurité des Systèmes Électroniques Embarqués"
event_url = ""
location = "Rennes, France"
selected = false
url_pdf = ""
url_slides = "slides/FaultsSoCs45.pdf"
url_video = ""
+++