+++
date = "2021-09-13"
math = false
title = "Can we solve timing side-channels in the CPU ?"
abstract = ""
abstract_short = ""
event = "TASER"
event_url = ""
location = "Online"
selected = false
url_pdf = ""
url_slides = "slides/TASER2021.pdf"
url_video = ""
+++