+++
date = "2019-10-16"
math = false
title = "Electromagnetic fault injection against a System-on-Chip"
abstract = ""
abstract_short = ""
event = "PHISIC2019"
event_url = ""
location = "Gardanne, France"
selected = false
url_pdf = ""
url_slides = "slides/FaultsSoCs20_2.pdf"
url_video = ""
+++