+++
date = "2017-01-26"
math = false
title = "A Multi-Round Side Channel Attack on AES using Belief Propagation"
abstract = ""
abstract_short = ""
event = "Conference on cybersecurity WG6"
event_url = ""
location = "Gardanne, France"
selected = false
url_pdf = ""
url_slides = "slides/FPS2016.pdf"
url_video = ""
+++
